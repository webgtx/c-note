all: compile execute clean

compile:
	clang $(source) -o x

debug:
	clang -g $(source) -o x
	gdb x
	rm x

execute:
	./x

clean:
	rm x
