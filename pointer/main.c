#include <stdbool.h>
#include <stdio.h>
int main() {
  int num = 1;
  int *pNum = &num;
  printf("%p \n", &pNum);
  printf("%d \n", *pNum);
  *pNum = 5;
  printf("%d \n", *pNum);
  return 0;
}
