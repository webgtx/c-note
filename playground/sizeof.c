#include <stdio.h>

int main() {
  int i = 555999;
  char *s = "Blah blah blah";
  char c = 'j';
  printf("Number: %zu\n"
         "String: %zu\n"
         "Char: %zu\n",
         sizeof(i), sizeof(s), sizeof(c));
}
