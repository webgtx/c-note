#include <stdio.h>
#define buffer 255

typedef struct {
  int x;
  int y;
} point;

void draw(point p);
void cuthalf(point *p);
void addhalf(point *p);

int main() {
  point pnt = {10, 10};
  draw(pnt);
  addhalf(&pnt);
  draw(pnt);
  cuthalf(&pnt);
  draw(pnt);
}

void cuthalf(point *p) {
  (*p).x = (*p).x / 2;
  (*p).y = (*p).y / 2;
}

void addhalf(point *p) {
  p->x += p->x / 2;
  p->y += p->y / 2;
}

void draw(point p) {
  if (p.x > buffer || p.y > buffer) {
    puts("You can't exceed buffer size of 255");
    return;
  }
  printf("[ x: %d\ty: %d ]\n", p.x, p.y);
  for (int y_cursor = 0; y_cursor < p.y; y_cursor++) {
    putchar('\t');
    for (int x_cursor = 0; x_cursor < p.x; x_cursor++) {
      printf("%c ", '$');
    }
    putchar('\n');
  }
}
