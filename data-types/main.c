#include <stdbool.h>
#include <stdio.h>

int main() {
  const int adminLog;

  bool is_a_human = true;
  short s_num = 32000;
  unsigned short us_num = 64000;
  int num = 1;
  long l_num = 32000000;
  long long ll_num;
  float f_num = 4.5f;
  double df_num = 4.6217384f;
  //  Chars
  //  -128 to 127
  char sym = 'a';

  int x = 5, y = 10, res = -1;

  x += y;

  res++;

  res = x + y;
  res = x - y;
  res = x / y;
  res = x * y;

  printf("Variable: %d + %d = %d \n", x, y, res);
  return 0;
}
